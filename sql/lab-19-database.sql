-- You may run this script against lab-19-database.db to re-initialize the database.

drop table if exists users;

create table users (

    id integer not null primary key,
    username varchar(64) unique not null,
    password varchar(64) not null,
    name varchar(64)

);

insert into users (id, username, password, name) values
    (1, 'user1', 'pa55word', 'Alice'),
    (2, 'user2', 'pa55word', 'Bob');


drop table if exists messages;

create table messages (

    id integer not null primary key,
	dtm timestamp,
    content varchar(256) not null,
    sentId integer not null,
    receivedId integer not null,
	FOREIGN KEY(sentId) REFERENCES users(id),
	FOREIGN KEY(receivedId) REFERENCES users(id)

);



insert into messages (id, dtm, content, sentId, receivedId) values
    (1, datetime('now'), 'hi', '3','4')