Web Lab 19 &ndash; SQLite3 access from node.js
==========
Begin by forking this repository into your namespace by clicking the `fork` button above, then selecting your username from the resulting window. Once completed, click the `clone` button, copy the `Clone with HTTPS` value. Then, clone your fork onto your local machine using the `git clone` command.

Remember to commit and push your work regularly for backup purposes. Practicing branching and merging is also highly recommended!

**Note:** Whenever this lab mentions opening a "terminal window", any terminal *should* work. However, the work has only been tested using `git bash`.

**Note 2:** Remember to run `npm install` for each webapp, to make sure all necessary packages (including Express and Handlebars) are installed.

**Note 4:** You *must work in your project teams* for this lab!

**Note 5:** When testing your `node.js` code, **make sure to close your database within the DB Browser tool**. If you don't, you may get *"database locked"* errors.


Exercise One &ndash; Understanding
-----------------------------
In this and following exercises, we'll gain familiarity with the `sqlite` and `sql-template-strings` packages, which can be used to easily access and interact with SQLite3 databases from within node.js. We'll also practice proper *separation of concerns* by organizing our code into appropriate modules.

To begin, we'll examine the existing project, which provides identical functionality to the login page from lab 16. However, this project uses a database, rather than a JSON file, to store user information.

Examine the project and gain an understanding of how it functions - particularly focusing on those parts of the code which access the database. More information about the project can be found in [project-info.md](./project-info.md).

Once you've gotten a sense of what the program does, and have successfully run and tested it, move on to Exercise Two.


Exercise Two &ndash; Account creation
-----------------------------
In this exercise, we'll add account creation functionality to the webapp. Perform the following steps:

1. Add a new button or hyperlink to the login page, which displays text such as "create new account", and links to `/newAccount`.

2. Add a new route handler to the `login-routes` module, which handles `GET` requests to `/newAccount`. The handler should simply render the `new-account` view.

3. Add a new route handler to the `login-routes` module, which handles `POST` requests to `/newAccount`. The handler should read the submitted name, username, and password from the form, and create a new user in the system. This can be done with the `createUser()` function in the `users-dao` module. Investigate this function to see the parameters it expects and how they are used. Once the new user has been created, the route handler should redirect to `/login`, supplying a message such as "Account created successfully!".

4. **Investigation:** The database is setup to only allow `unique` usernames. If someone tries to create an account with an already-existing username, this will fail. As it stands though, the user experience isn't great - the webpage will simply "hang" until they press the "stop" button - then an error message such as `SQLITE_CONSTRAINT: UNIQUE constraint failed: users.username` will be visible on the server console. You may try and remedy this by adding a `try / catch` around your call to `createUser()`. Upon catching an error, redirect the user back to the account creation page with an appropriate error message (and modify the view so this message is visible). Investigate `try / catch` here: <https://www.w3schools.com/js/js_errors.asp>.

Exercise Three &ndash; Messages table
-----------------------------
Design a new database table intended to store messages which one user sends to another. The messages table should include the following information:

- A unique id
- The date &amp; time the mesage was sent (timestamp)
- The content of the message (256 characters max)
- The `id` of the user who sent the message
- The `id` of the user who received the message

Remember when designing the table, to consider any primary key, foreign key and other constraints which might be necessary.

Create the table in your `lab-19-database.db` database, and add some dummy data to it within the DB Browser tool. You may also wish to modify `lab-19-database.sql` to include the definition of this new table, along with some dummy data.

**Hints:**

- The *data type* of the the date and time column can be `timestamp`.

- To add data to a column of that type, you can use the `datetime()` *SQL function*. For example:
  - `datetime('2019-10-15 15:00:00')` refers to the specific time of 3pm on the 15th of October, 2019
  - `datetime('now')` refers to whatever the current date and time is.

  For more examples, see the following link: <https://www.sqlite.org/lang_datefunc.html>


Exercise Four &ndash; Messages DAO
-----------------------------
For this exercise, create a *DAO module* similar to `users-dao.js`, but for dealing with *messages* instead of users (i.e. from the table you just created in Exercise Three).

Create a new module, called `messages-dao.js`, and add (and export) functions which perform the following tasks:

1. Create a new message, given a `senderId`, `receiverId`, and `content` as parameters. The timestamp should be set to the current time (see the hints for Exercise Three above), and the id should be auto-generated by the database.

2. Retrieve all messages which were *received by* a given user (given that user's `id` as a parameter). Messages should be sorted by timestamp, in *descending order* (i.e. most recent message first). In addition to the information contained within your messages table, each row in the result should also contain the `username` of the `sender` (so this can be displayed in Exercise Five). You should be able to achieve this with *one `SELECT` statement* (with a join).

3. Deletes a message, given its `id` as a parameter.


Exercise Five &ndash; Messages view on homepage
-----------------------------
For this exercise, add code to the `home` view and `/` route handler which will display all messages received by a user on their homepage. The exact design of the HTML / CSS is up to you, but for each message, its timestamp, sender username, and content should be displayed.


Exercise Six &ndash; Sending messages
-----------------------------
Finally, add all frontend and backend code which will allow users to send messages. The frontend should comprise of a form on a user's homepage, which should at minimum contain a text area to enter the content, and a text input to enter the receiver id. A much better solution would be to allow the sender to enter the *username* of the receiver, rather than their id - so aim for this if possible.

If trying to send a message to a nonexistent user, an appropriate error message should be displayed.