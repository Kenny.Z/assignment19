const express = require("express");
const router = express.Router();

const verifyAuthenticated = require("../modules/verify-auth.js");

// Whenever we navigate to /, verify that we're authenticated. If we are, render the home view.
router.get("/", verifyAuthenticated, function(req, res) {

    res.render("home");
});

module.exports = router;